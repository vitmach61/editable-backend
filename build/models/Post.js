"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var PostSchema = new mongoose_1.Schema({
    createdAt: Date,
    updatedAt: Date,
    type: {
        type: Number,
        default: '',
        required: true
    },
    fid: {
        type: String,
        default: '',
        required: true,
        unique: true
    },
    content: [{
            type: Object
        }],
    children: [{
            type: String,
            //ref: 'Post',
            unique: false
        }],
    text: {
        type: String,
        default: ''
    },
    src: {
        type: String,
        default: ''
    },
    page: {
        type: String,
        default: '',
        required: true
    },
    href: {
        type: String,
        default: ''
    }
});
exports.default = mongoose_1.model('Post', PostSchema);
