"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Post_1 = require("../models/Post");
var PostRouter = /** @class */ (function () {
    function PostRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    PostRouter.prototype.GetPosts = function (req, res) {
        Post_1.default.find({})
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.GetPage = function (req, res) {
        Post_1.default.find({ page: 'global' })
            .then(function (data) {
            //console.log(data);
            var sendData;
            var arrMap = new Map;
            data.forEach(function (element) {
                arrMap.set(element.fid, element);
            });
            function recFc(fid) {
                if (arrMap.has(fid)) {
                    var base = void 0;
                    base = arrMap.get(fid);
                    arrMap.delete(fid);
                    base.content = base.children.map(function (fid) { return (recFc(fid)); });
                    return base;
                }
            }
            sendData = recFc(0 + "");
            console.log(sendData);
            var status = res.statusCode;
            res.json({
                status: status,
                sendData: sendData
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.GetContent = function (req, res) {
        var cont = req.params.cont;
        console.log(cont);
        Post_1.default.find({ page: cont })
            .then(function (data) {
            console.log(data);
            var sendData;
            var arrMap = new Map;
            var baseFid = null;
            data.forEach(function (element) {
                arrMap.set(element.fid, element);
                if (element.type == 3) {
                    baseFid = element.fid;
                }
            });
            function recFc(fid) {
                console.log(fid);
                if (arrMap.has(fid)) {
                    console.log(fid, "exists");
                    var base = void 0;
                    base = arrMap.get(fid);
                    arrMap.delete(fid);
                    base.content = base.children.map(function (fid) { return (recFc(fid)); });
                    return base;
                }
            }
            if (baseFid !== null)
                sendData = recFc(baseFid + "");
            else
                sendData = "err: no base element.";
            //console.log(sendData);
            var status = res.statusCode;
            res.json({
                status: status,
                sendData: sendData
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.GetPost = function (req, res) {
        var fid = req.params.fid;
        Post_1.default.findOne({ fid: fid })
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.CreatePost = function (req, res) {
        var type = req.body.type;
        var fid = req.body.fid;
        var content = req.body.content;
        var text = req.body.text;
        var children = req.body.children;
        var src = req.body.src;
        var page = req.body.page;
        var href = req.body.href;
        var post = new Post_1.default({
            type: type,
            fid: fid,
            content: content,
            text: text,
            children: children,
            src: src,
            page: page,
            href: href
        });
        post.save()
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.UpdatePost = function (req, res) {
        var fid = req.params.fid;
        Post_1.default.findOneAndUpdate({ fid: fid }, req.body)
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.DeletePost = function (req, res) {
        var fid = req.params.fid;
        Post_1.default.findOneAndDelete({ fid: fid })
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            var status = res.statusCode;
            res.json({
                status: status,
                err: err
            });
        });
    };
    PostRouter.prototype.routes = function () {
        this.router.get('/page', this.GetPage);
        this.router.get('/content/:cont', this.GetContent);
        this.router.get('/', this.GetPosts);
        this.router.get('/:fid', this.GetPost);
        this.router.post('/', this.CreatePost);
        this.router.put('/:fid', this.UpdatePost);
        this.router.delete('/:fid', this.DeletePost);
    };
    return PostRouter;
}());
exports.default = new PostRouter();
