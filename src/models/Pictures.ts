import {Schema, model} from "mongoose";

let Pictures: Schema = new Schema({
    createdAt: Date,
    updatedAt: Date,
    name:{
        type: String,
        default: ''
    },
    src: {
        type: String,
        default: ''
    }
});

export default model('Pictures', Pictures);