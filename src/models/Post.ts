import {Schema, model} from "mongoose";

let PostSchema: Schema = new Schema({
    createdAt: Date,
    updatedAt: Date,
    type:{
        type: Number,
        default: '',
        required: true
    },
    fid: {
        type: String,
        default: '',
        required: true,
        unique: true
    },
    content: [{
        type: Object
    }],
    children: [{
        type: String,
        //ref: 'Post',
        unique: false
    }],
    text: {
        type: String,
        default: ''
    },
    src: {
        type: String,
        default: ''
    },
    page: {
        type: String,
        default: '',
        required: true
    },
    href: {
        type: String,
        default: ''
    }
});

export default model('Post', PostSchema);