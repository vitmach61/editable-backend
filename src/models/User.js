"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
var UserSchema = new mongoose_1.Schema({
    createdAt: Date,
    updatedAt: Date,
    name: {
        type: String,
        "default": '',
        required: true
    }, UserName: {
        type: String,
        "default": '',
        required: true,
        unique: true,
        lowercase: true
    },
    email: {
        type: String,
        "default": '',
        required: true,
        unique: true
    },
    password: {
        type: String,
        "default": '',
        required: true
    }
});
exports["default"] = mongoose_1.model('Post', UserSchema);
