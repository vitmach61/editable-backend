import { Router, Request, Response, NextFunction } from 'express';
import Pictures from "../models/Pictures";
const multer  = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
});

/*const fileFilter = (req, file, cb) => {
    if(file.mimeType === 'image/jpeg' || file.mimeType === 'image/gif')
        cb(null, true);
    else
        cb(null, false);
};*/

const upload = multer({
    storage: storage
});

class PicturesRouter {
    router: Router;

    constructor(){
        this.router = Router();
        this.routes();
    }

    public GetPictures(req: Request, res: Response): void {
        Pictures.find({})
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public GetPicture(req: Request, res: Response): void {
        const name: string = req.params.name;

        Pictures.findOne({ name })
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;

                res.json({
                    status,
                    err
                });
            })
    }

    public CreatePicture(req: Request, res: Response): void {
        console.log(req.file);
        const name: string = req.file.originalname;
        let src: string = "";
        if (req.file !== undefined) {
            src = req.file.path;
        }


        const pictures = new Pictures({
            name,
            src
        });

        pictures.save()
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public UpdatePicture(req: Request, res: Response): void {
        const name: string = req.params.name;

        Pictures.findOneAndUpdate({ name }, req.body)
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public DeletePicture(req: Request, res: Response): void {
        const name: string = req.params.name;

        Pictures.findOneAndDelete({ name })
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public routes(){
        this.router.get('/', this.GetPictures);
        this.router.get('/:name', this.GetPicture);
        this.router.post('/', upload.single('product'), this.CreatePicture);
        this.router.put('/:name', this.UpdatePicture);
        this.router.delete('/:name', this.DeletePicture);
    }
}

export default new PicturesRouter();