import { Router, Request, Response, NextFunction } from 'express';
import Post from "../models/Post";

class PostRouter {
    router: Router;

    constructor(){
        this.router = Router();
        this.routes();
    }

    public GetPosts(req: Request, res: Response): void {
        Post.find({})
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public GetPage(req: Request, res: Response): void {
        Post.find({page: 'global'})
            .then((data)=>{
                //console.log(data);
                let sendData;
                let arrMap = new Map;

                data.forEach(function(element) {
                   arrMap.set(element.fid, element);
                });

                function recFc(fid){
                    if (arrMap.has(fid)){
                        let base;
                        base = arrMap.get(fid);
                        arrMap.delete(fid);
                        base.content = base.children.map(fid => (
                            recFc(fid)
                        ));
                        return base;
                    }
                }

                sendData = recFc(0+"");

                console.log(sendData);

                const status = res.statusCode;
                res.json({
                    status,
                    sendData
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public GetContent(req: Request, res: Response): void {

            const cont: string = req.params.cont;
            console.log(cont);

        Post.find({page: cont})
            .then((data)=>{
                console.log(data);
                let sendData;
                let arrMap = new Map;
                let baseFid = null;
                data.forEach(function(element) {
                    arrMap.set(element.fid, element);
                    if( element.type == 3){
                        baseFid = element.fid;
                    }
                });

                function recFc(fid){
                    console.log(fid);
                    if (arrMap.has(fid)){
                        console.log(fid, "exists");
                        let base;
                        base = arrMap.get(fid);
                        arrMap.delete(fid);
                        base.content = base.children.map(fid => (
                            recFc(fid)
                        ));
                        return base;
                    }
                }
                if (baseFid !== null)
                    sendData = recFc(baseFid+"");
                else
                    sendData = "err: no base element.";

                //console.log(sendData);

                const status = res.statusCode;
                res.json({
                    status,
                    sendData
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public GetPost(req: Request, res: Response): void {
        const fid: string = req.params.fid;

        Post.findOne({ fid })
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;

                res.json({
                    status,
                    err
                });
            })
    }

    public CreatePost(req: Request, res: Response): void {
        const type: string = req.body.type;
        const fid: string = req.body.fid;
        const content: string = req.body.content;
        const text: string = req.body.text;
        const children: string = req.body.children;
        const src: string = req.body.src;
        const page: string = req.body.page;
        const href: string = req.body.href;

        const post = new Post({
            type,
            fid,
            content,
            text,
            children,
            src,
            page,
            href
        });

        post.save()
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public UpdatePost(req: Request, res: Response): void {
        const fid: string = req.params.fid;

        Post.findOneAndUpdate({ fid }, req.body)
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

    public DeletePost(req: Request, res: Response): void {
        const fid: string = req.params.fid;

        Post.findOneAndDelete({ fid })
            .then((data)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err)=>{
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            })
    }

     public routes(){
         this.router.get('/page', this.GetPage);
         this.router.get('/content/:cont', this.GetContent);
         this.router.get('/', this.GetPosts);
         this.router.get('/:fid', this.GetPost);
         this.router.post('/', this.CreatePost);
         this.router.put('/:fid', this.UpdatePost);
         this.router.delete('/:fid', this.DeletePost);
    }
}

export default new PostRouter();